package deriva.camera.controller;

import java.security.Principal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Null;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import deriva.camera.domain.DerivaUser;
import deriva.camera.repository.UserRepository;
import deriva.camera.service.CounterLogService;

@RestController
@RequestMapping("/overview")
//@Secured({"ROLE_ADMIN","ROLE_USER","ROLE_BUSINESS","ROLE_DEVELOPER","ROLE_OPERATION","ROLE_BRANDMANAGER","ROLE_BRANDUSER","ROLE_BRANCHUSER"})
public class CounterLogController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CounterLogService counterLogService;

    @RequestMapping(value = "/totalVisitor/start={startDate}&end={endDate}&clientId={clientId}&branches={branches}", method = RequestMethod.GET)
    public int totalVisitorReport(@PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate, @PathVariable("clientId") int clientId, @PathVariable("branches") String branches, HttpServletRequest request) {
        //System.out.println(request.getAuthType());
        Principal principal = request.getUserPrincipal();
        DerivaUser user = null;
        try {
            user = userRepository.getUserInfo(principal.getName());
        } catch (SQLException | NullPointerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())) + " " +principal.getName() + " /overview/totalVisitor/start={"+startDate+"}&end={"+endDate+"}&clientId={"+clientId+"}" + request.getRemoteAddr());

        if ((user != null && user.getClientId() == clientId) || user.getClientId() == 0) {
            try {
                return counterLogService.getTotalVisitor(clientId, startDate, endDate,branches);
            } catch (SQLException | NullPointerException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    @RequestMapping(value = "/averageTime/start={startDate}&end={endDate}&clientId={clientId}&branches={branches}", method = RequestMethod.GET)
    public String averageTimeReport(@PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate, @PathVariable("clientId") int clientId,@PathVariable("branches") String branches, HttpServletRequest request) {
        //System.out.println(request.getAuthType());
        Principal principal = request.getUserPrincipal();
        DerivaUser user = null;
        try {
            user = userRepository.getUserInfo(principal.getName());
        } catch (SQLException | NullPointerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())) + " " +principal.getName() + " /branch/report/detail " + request.getRemoteAddr());

        if ((user != null && user.getClientId() == clientId) || user.getClientId() == 0) {
            try {
                return counterLogService.getAverageTime(clientId, startDate, endDate,branches);
            } catch (SQLException | NullPointerException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @RequestMapping(value = "/crowdPerHour/start={startDate}&end={endDate}&clientId={clientId}&branches={branches}", method = RequestMethod.GET)
    public Map<String, Integer> crowdPerHourReport(@PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate, @PathVariable("clientId") int clientId, @PathVariable("branches") String branches, HttpServletRequest request) {
        //System.out.println(request.getAuthType());
        Principal principal = request.getUserPrincipal();
        DerivaUser user = null;
        try {
            user = userRepository.getUserInfo(principal.getName());
        } catch (SQLException | NullPointerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())) + " " +principal.getName() + " /branch/report/detail " + request.getRemoteAddr());

        if ((user != null && user.getClientId() == clientId) || user.getClientId() == 0) {
            try {
                return counterLogService.getCrowdPerHour(clientId, startDate, endDate,branches);
            } catch (SQLException | NullPointerException e) {
                e.printStackTrace();
            }
        }
        return new HashMap<>();
    }

    @RequestMapping(value = "/avgTimePerHour/start={startDate}&end={endDate}&clientId={clientId}&branches={branches}", method = RequestMethod.GET)
    public Map<String, String> avgTimePerHourReport(@PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate, @PathVariable("clientId") int clientId, @PathVariable("branches") String branches, HttpServletRequest request) {
        //System.out.println(request.getAuthType());
        Principal principal = request.getUserPrincipal();
        DerivaUser user = null;
        try {
            user = userRepository.getUserInfo(principal.getName());
        } catch (SQLException | NullPointerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())) + " " +principal.getName() + " /branch/report/detail " + request.getRemoteAddr());

        if ((user != null && user.getClientId() == clientId) || user.getClientId() == 0) {
            try {
                return counterLogService.getAvgTimePerHour(clientId, startDate, endDate,branches);
            } catch (SQLException | NullPointerException e) {
                e.printStackTrace();
            }
        }
        return new HashMap<>();
    }

    @RequestMapping(value = "/crowdPerBranch/start={startDate}&end={endDate}&clientId={clientId}&branches={branches}", method = RequestMethod.GET)
    public HashMap<String, Integer> crowdPerBranchReport(@PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate, @PathVariable("clientId") int clientId, @PathVariable("branches") String branches, HttpServletRequest request) {
        //System.out.println(request.getAuthType());
        Principal principal = request.getUserPrincipal();
        DerivaUser user = null;
        try {
            user = userRepository.getUserInfo(principal.getName());
        } catch (SQLException | NullPointerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())) + " " +principal.getName() + " /branch/report/detail " + request.getRemoteAddr());

        if ((user != null && user.getClientId() == clientId) || user.getClientId() == 0) {
            try {
                return counterLogService.getCrowdPerBranch(clientId, startDate, endDate,branches);
            } catch (SQLException | NullPointerException e) {
                e.printStackTrace();
            }
        }
        return new HashMap<>();
    }

    @RequestMapping(value = "/avgTimePerBranch/start={startDate}&end={endDate}&clientId={clientId}&branches={branches}", method = RequestMethod.GET)
    public HashMap<String, String> avgTimePerBranchReport(@PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate, @PathVariable("clientId") int clientId, @PathVariable("branches") String branches, HttpServletRequest request) {
        //System.out.println(request.getAuthType());
        Principal principal = request.getUserPrincipal();
        DerivaUser user = null;
        try {
            user = userRepository.getUserInfo(principal.getName());
        } catch (SQLException | NullPointerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())) + " " +principal.getName() + " /branch/report/detail " + request.getRemoteAddr());
System.out.println("branches : "+branches);
        if ((user != null && user.getClientId() == clientId) || user.getClientId() == 0) {
            try {
                return counterLogService.getAvgTimePerBranch(clientId, startDate, endDate,branches);
            } catch (SQLException | NullPointerException e) {
                e.printStackTrace();
            }
        }
        return new HashMap<>();
    }
}
