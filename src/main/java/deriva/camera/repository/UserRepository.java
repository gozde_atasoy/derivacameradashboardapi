package deriva.camera.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import deriva.camera.config.DataManager;
import deriva.camera.domain.DerivaUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {

    public String databaseIP = "136.243.144.166";
    public String databaseName = "deriva_main";

    public DerivaUser getUser(String email) throws SQLException {

        String query = "SELECT * FROM deriva_main.user WHERE email='" + email + "';";

        DataManager dm = new DataManager(this.databaseIP, this.databaseName);

        ResultSet resultSet = dm.select(query);

        DerivaUser user = null;

        if (resultSet.next()) {
            user = new DerivaUser(email, resultSet.getString("password"));
        }

        dm.close();

        return user;

    }

    public DerivaUser getUserInfo(String email) throws SQLException {
        //String query = "SELECT * FROM deriva_main.user u LEFT JOIN user_type ut ON u.userType=ut.typeId LEFT JOIN user_client uc ON u.userId=uc.userId WHERE email='" + email + "';";

        String query = ""
                + " SELECT "
                + " 	 u.*,ut.*,uc.*,c.isDemo,DATE(c.createdAt) as reportStartDate "
                + " FROM deriva_main.user u "
                + " LEFT JOIN deriva_main.user_type ut ON u.userType = ut.typeId "
                + " LEFT JOIN deriva_authorization.user_client uc ON u.userId = uc.userId "
                + " LEFT JOIN deriva_main.client c ON uc.clientId = c.clientId "
                + " WHERE "
                + "	email='" + email + "'; ";

        DataManager dm = new DataManager(this.databaseIP, this.databaseName);

        ResultSet resultSet = dm.select(query);

        ArrayList<String> authorities = new ArrayList<>();

        Collection<? extends GrantedAuthority> grantedAuthorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();

        if(grantedAuthorities != null){
            for (GrantedAuthority grantedAuthority: grantedAuthorities) {
                if(grantedAuthority != null){
                    authorities.add(grantedAuthority.getAuthority());
                }
            }
        }


        DerivaUser user = null;

        if (resultSet.next()) {
            user = new DerivaUser(resultSet.getInt("userId"), resultSet.getString("email"), resultSet.getString("firstName")
                    , resultSet.getString("lastName"), "", resultSet.getString("createdAt"), resultSet.getString("updatedAt")
                    , resultSet.getInt("userType"), resultSet.getInt("clientId"), resultSet.getInt("authLevel"), resultSet.getString("reportStartDate"), resultSet.getInt("isDemo"),authorities);
        }

        dm.close();

        return user;
    }


    public DerivaUser getAuthenticatedUser(String email) {
        DataManager dm = new DataManager(databaseIP, databaseName);

        String query = "SELECT * FROM deriva_main.user u LEFT JOIN deriva_main.user_type t ON t.typeId=u.userType  LEFT JOIN user_client c ON u.userId = c.userId WHERE u.email='"
                + email + "';";

        ResultSet resultSet = dm.select(query);

        try {
            if (resultSet.next()) {
                System.out.println(resultSet.getString("typeName"));
                return new DerivaUser(resultSet.getInt("userId"), email, resultSet.getInt("clientId"),
                        resultSet.getInt("authLevel"));
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            dm.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return new DerivaUser();
    }

    public int resetPassword(String username, String password) {

        DataManager dm = new DataManager(databaseIP, databaseName);

        String query = "UPDATE deriva_main.user SET password = '" + password + "' WHERE email='" + username + "';";

        return dm.update(query);
    }

}
