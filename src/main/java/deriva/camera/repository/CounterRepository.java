package deriva.camera.repository;

public class CounterRepository {

	public static String totalVisitorQuery(String branches,String startDate, String endDate){
		
		String query = ""
				+"SELECT SUM(countIn) as totalVisitor "
				+"FROM count_log cl "
				+"	LEFT JOIN deriva_main.counter_device cd ON cd.channelId = cl.channelId "
				+"WHERE DATE(countDate) BETWEEN '"+startDate+"' AND '"+endDate+"' "
				+"	AND cd.branchId IN ("+branches+") "
				+"	AND HOUR(countDate) BETWEEN 10 AND 21 ";
		
		return query;
	}
	
	public static String averageTimeQuery(String branches,String startDate, String endDate){
		
		String query = ""
				+"SELECT "
				+"	SEC_TO_TIME(ROUND((SUM(countOut)+SUM(countInside))/SUM(countIn))*60) as avgTime "
				+"FROM count_log cl "
				+"	LEFT JOIN deriva_main.counter_device cd ON cd.channelId = cl.channelId "
				+"WHERE DATE(countDate) BETWEEN '"+startDate+"' AND '"+endDate+"' "
				+"	AND cd.branchId IN ("+branches+") "
				+"	AND HOUR(countDate) BETWEEN 10 AND 21 ";
		
		return query;
	}
	
	public static String crowdPerHourQuery(String branches,String startDate, String endDate){
		
		String query = ""
				+"SELECT "
				+"	SUM(countIn) as totalVisitor,"
				+"	HOUR(countDate) as hour "
				+"FROM count_log cl "
				+"	LEFT JOIN deriva_main.counter_device cd ON cd.channelId = cl.channelId "
				+"WHERE DATE(countDate) BETWEEN '"+startDate+"' AND '"+endDate+"' "
				+"	AND cd.branchId IN ("+branches+") "
				+"	AND HOUR(countDate) BETWEEN 10 AND 21 "
				+"GROUP BY HOUR(countDate) ";
		
		return query;
	}
	
	public static String avgTimePerHourQuery(String branches,String startDate, String endDate){
		
		String query = ""
				+"SELECT  "
				+"	SEC_TO_TIME(ROUND((SUM(countOut)+SUM(countInside))/SUM(countIn))*60) as avgTime , "
				+"	HOUR(countDate) as hour "
				+"FROM count_log cl "
				+"	LEFT JOIN deriva_main.counter_device cd ON cd.channelId = cl.channelId "
				+"WHERE DATE(countDate) BETWEEN '"+startDate+"' AND '"+endDate+"' "
				+"	AND cd.branchId IN ("+branches+") "
				+"	AND HOUR(countDate) BETWEEN 10 AND 21 "
				+"GROUP BY HOUR(countDate) ";
		
		return query;
	}
	
	public static String crowdPerBranchQuery(String branches,String startDate, String endDate){
		
		String query = ""
				+"SELECT  "
				+"	IFNULL(total.totalVisitor,0) as totalVisitor ,branch.branchName  "
				+"FROM ( "
				+"	SELECT  "
				+"		SUM(countIn) as totalVisitor , cd.branchId  "
				+"	FROM count_log cl "
				+"		LEFT JOIN deriva_main.counter_device cd ON cd.channelId = cl.channelId "
				+"	WHERE DATE(countDate) BETWEEN '"+startDate+"' AND '"+endDate+"' "
				+"		AND cd.branchId IN ("+branches+") "
				+"		AND HOUR(countDate) BETWEEN 10 AND 21 "
				+"	GROUP BY cd.branchId  "
				+") total "
				+"RIGHT JOIN( "
				+"	SELECT * FROM deriva_main.branch WHERE branchId IN ("+branches+") "
				+") branch "
				+"ON branch.branchId = total.branchId ";
		
		return query;
	}
	
	public static String avgTimePerBranchQuery(String branches,String startDate, String endDate){
		
		String query = ""
				+"SELECT IFNULL(avgTime.avgTime,'00:00:00') as avgTime , branch.branchName FROM ( "
				+"	SELECT  "
				+"		SEC_TO_TIME(ROUND((SUM(countOut)+SUM(countInside))/SUM(countIn))*60) as avgTime ,  "
				+"		cd.branchId "
				+"	FROM count_log cl "
				+"	LEFT JOIN deriva_main.counter_device cd ON cd.channelId = cl.channelId "
				+"	WHERE DATE(countDate) BETWEEN '"+startDate+"' AND 	'"+endDate+"' "
				+"		AND cd.branchId IN ("+branches+") "
				+"		AND HOUR(countDate) BETWEEN 10 AND 21 "
				+"	GROUP BY cd.branchId "
				+")avgTime "
				+"RIGHT JOIN ( "
				+"	SELECT * FROM deriva_main.branch WHERE branchId IN ("+branches+") "
				+")branch "
				+"ON branch.branchId = avgTime.branchId ";
		
		return query;
	}
}
