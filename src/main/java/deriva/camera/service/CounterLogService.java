package deriva.camera.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import deriva.camera.config.DataManager;
import deriva.camera.repository.CounterRepository;

import org.springframework.stereotype.Service;


@Service
public class CounterLogService {
	
	public int getTotalVisitor(int clientId,String startDate, String endDate , String branches) throws SQLException{

		DataManager dm = new DataManager(clientId);
		ResultSet rs = null;
		int totalVisitor = 0;
		if(branches.equals("")){
			branches = "SELECT branchId FROM deriva_main.branch WHERE clientId ="+clientId;
		}
		rs = dm.select(CounterRepository.totalVisitorQuery(branches, startDate, endDate));
		
		if(rs.next()){
			totalVisitor = rs.getInt("totalVisitor");
		}
		
		return totalVisitor;
	}
	
	public String getAverageTime(int clientId,String startDate, String endDate, String branches) throws SQLException{

		DataManager dm = new DataManager(clientId);
		ResultSet rs = null;
		String averageTime = "";
		if(branches.equals("")){
			branches = "SELECT branchId FROM deriva_main.branch WHERE clientId ="+clientId;
		}
		rs = dm.select(CounterRepository.averageTimeQuery(branches, startDate, endDate));
		
		if(rs.next()){
			averageTime = rs.getString("avgTime");
		}
		
		return averageTime;
	}
	
	public Map<String, Integer> getCrowdPerHour(int clientId,String startDate, String endDate, String branches) throws SQLException{

		DataManager dm = new DataManager(clientId);
		ResultSet rs = null;
		Map<String, Integer> crowdPerHourMap = new TreeMap<>();
		
		for (int i = 10; i < 22; i++) {
			crowdPerHourMap.put(i+":00", 0);
		}
		if(branches.equals("")){
			branches = "SELECT branchId FROM deriva_main.branch WHERE clientId ="+clientId;
		}
		rs = dm.select(CounterRepository.crowdPerHourQuery(branches, startDate, endDate));
		
		while(rs.next()){
			crowdPerHourMap.put(rs.getString("hour")+":00", rs.getInt("totalVisitor"));
		}
		
		return crowdPerHourMap;
	}
	
	public Map<String, String> getAvgTimePerHour(int clientId,String startDate, String endDate, String branches) throws SQLException{

		DataManager dm = new DataManager(clientId);
		ResultSet rs = null;
		Map<String, String> avgTimePerHourMap = new TreeMap<>();
		for (int i = 10; i < 22; i++) {
			avgTimePerHourMap.put(i+":00", "00:00:00");
		}
		if(branches.equals("")){
			branches = "SELECT branchId FROM deriva_main.branch WHERE clientId ="+clientId;
		}
		rs = dm.select(CounterRepository.avgTimePerHourQuery(branches, startDate, endDate));
		
		while(rs.next()){
			avgTimePerHourMap.put(rs.getString("hour")+":00", rs.getString("avgTime"));
		}
		
		return avgTimePerHourMap;
	}
	
	public HashMap<String, Integer> getCrowdPerBranch(int clientId,String startDate, String endDate, String branches) throws SQLException{

		DataManager dm = new DataManager(clientId);
		ResultSet rs = null;
		HashMap<String, Integer> crowdPerBranchMap = new HashMap<>();
		if(branches.equals("")){
			branches = "SELECT branchId FROM deriva_main.branch WHERE clientId ="+clientId;
		}
		rs = dm.select(CounterRepository.crowdPerBranchQuery(branches, startDate, endDate));
		
		while(rs.next()){
			crowdPerBranchMap.put(rs.getString("branch.branchName"), rs.getInt("totalVisitor"));
		}
		
		return crowdPerBranchMap;
	}
	
	public HashMap<String, String> getAvgTimePerBranch(int clientId,String startDate, String endDate, String branches) throws SQLException{

		DataManager dm = new DataManager(clientId);
		ResultSet rs = null;
		HashMap<String, String> avgTimePerBranchMap = new HashMap<>();
		if(branches.equals("") || branches.equals(null)){
			branches = "SELECT branchId FROM deriva_main.branch WHERE clientId ="+clientId;
		}
		rs = dm.select(CounterRepository.avgTimePerBranchQuery(branches, startDate, endDate));
		
		while(rs.next()){
			avgTimePerBranchMap.put(rs.getString("branch.branchName"), rs.getString("avgTime"));
		}
		
		return avgTimePerBranchMap;
	}
}
