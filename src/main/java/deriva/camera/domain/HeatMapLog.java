package deriva.camera.domain;

public class HeatMapLog {

	private int heatmapId;
	private byte[] heatmapImg;
	private String heatmapDate;
	private String planId;
	private String serverId;
	private String createdAt;
	
	public HeatMapLog(int heatmapId, byte[] heatmapImg, String heatmapDate, String planId, String serverId,
			String createdAt) {
		super();
		this.heatmapId = heatmapId;
		this.heatmapImg = heatmapImg;
		this.heatmapDate = heatmapDate;
		this.planId = planId;
		this.serverId = serverId;
		this.createdAt = createdAt;
	}

	public int getHeatmapId() {
		return heatmapId;
	}

	public void setHeatmapId(int heatmapId) {
		this.heatmapId = heatmapId;
	}

	public byte[] getHeatmapImg() {
		return heatmapImg;
	}

	public void setHeatmapImg(byte[] heatmapImg) {
		this.heatmapImg = heatmapImg;
	}

	public String getHeatmapDate() {
		return heatmapDate;
	}

	public void setHeatmapDate(String heatmapDate) {
		this.heatmapDate = heatmapDate;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
}
