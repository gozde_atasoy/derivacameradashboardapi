package deriva.camera.domain;


import java.util.ArrayList;

public class DerivaUser {
	
	private int userId;
	private String email;
	private String firstName;
	private String lastName;
	private String password;
	private String createdAt;
	private String updatedAt;
	private int userType;
	private int clientId;
	private int authLevel;
	private int isValid;
	private int isDemo;
	private String typeName;
	private String reportStartDate;
	private ArrayList<String> authorities;
	
	/**
	 * 
	 */
	public DerivaUser() {
		super();
	}
	/**
	 * @param userId
	 * @param email
	 * @param firstName
	 * @param lastName
	 * @param password
	 * @param createdAt
	 * @param updatedAt
	 * @param userType
	 */
	public DerivaUser(int userId, String email, String firstName, String lastName, String password, String createdAt,
			String updatedAt, int userType) {
		super();
		this.userId = userId;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.userType = userType;
	}
	
	/**
	 * @param userId
	 * @param email
	 * @param firstName
	 * @param lastName
	 * @param password
	 * @param createdAt
	 * @param updatedAt
	 * @param userType
	 * @param clientId
	 * @param authLevel
	 * @param reportStartDate ( Start Date of membership )
	 */
	
	public DerivaUser(int userId, String email, String firstName, String lastName, String password, String createdAt,
			String updatedAt, int userType, int clientId, int authLevel,String reportStartDate) {
		super();
		this.userId = userId;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.userType = userType;
		this.clientId = clientId;
		this.authLevel = authLevel;
		this.reportStartDate = reportStartDate;
	}
	
	public DerivaUser(int userId, String email, String firstName, String lastName, String password, String createdAt,
			String updatedAt, int userType, int clientId, int authLevel, String reportStartDate, int isDemo,ArrayList<String> authorities) {
		super();
		this.userId = userId;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.userType = userType;
		this.clientId = clientId;
		this.authLevel = authLevel;
		this.reportStartDate = reportStartDate;
		this.isDemo = isDemo;
		this.authorities = authorities;
	}
	

	/**
	 * @param userId
	 * @param email
	 * @param firstName
	 * @param lastName
	 * @param password
	 * @param createdAt
	 * @param updatedAt
	 * @param userType
	 * @param clientId
	 * @param authLevel
	 * @param isValid
	 * @param typeName
	 */
	public DerivaUser(int userId, String email, String firstName, String lastName, String password, String createdAt,
			String updatedAt, int userType, int clientId, int authLevel, int isValid, String typeName) {
		super();
		this.userId = userId;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.userType = userType;
		this.clientId = clientId;
		this.authLevel = authLevel;
		this.isValid = isValid;
		this.typeName = typeName;
	}
	/**
	 * @param userId
	 * @param email
	 * @param firstName
	 * @param lastName
	 * @param password
	 * @param createdAt
	 * @param updatedAt
	 * @param userType
	 * @param clientId
	 * @param authLevel
	 * @param isValid
	 */
	public DerivaUser(int userId, String email, String firstName, String lastName, String password, String createdAt,
			String updatedAt, int userType, int clientId, int authLevel, int isValid) {
		super();
		this.userId = userId;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.userType = userType;
		this.clientId = clientId;
		this.authLevel = authLevel;
		this.isValid = isValid;
	}
	/**
	 * @param email
	 * @param password
	 */
	public DerivaUser(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	/**
	 * @param userId
	 * @param email
	 * @param clientId
	 * @param authLevel
	 */
	public DerivaUser(int userId, String email, int clientId, int authLevel) {
		super();
		this.userId = userId;
		this.email = email;
		this.clientId = clientId;
		this.authLevel = authLevel;
	}
	
	/**
	 * @param email
	 * @param clientId
	 * @param authLevel
	 */
	public DerivaUser(String email, int clientId, int authLevel) {
		super();
		this.email = email;
		this.clientId = clientId;
		this.authLevel = authLevel;
	}
	/**
	 * @param email
	 * @param firstName
	 * @param lastName
	 * @param password
	 * @param createdAt
	 * @param updatedAt
	 * @param userType
	 */
	public DerivaUser(String email, String firstName, String lastName, String password, String createdAt, String updatedAt,
			int userType) {
		super();
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.userType = userType;
	}
	
	
	
	/**
	 * @param email
	 * @param firstName
	 * @param lastName
	 * @param password
	 * @param createdAt
	 * @param updatedAt
	 * @param userType
	 * @param clientId
	 * @param authLevel
	 */
	public DerivaUser(String email, String firstName, String lastName, String password, String createdAt,
			String updatedAt, int userType, int clientId, int authLevel) {
		super();
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.userType = userType;
		this.clientId = clientId;
		this.authLevel = authLevel;
	}
	
	/**
	 * @param userId
	 * @param email
	 * @param firstName
	 * @param lastName
	 * @param password
	 * @param createdAt
	 * @param updatedAt
	 * @param userType
	 * @param clientId
	 * @param authLevel
	 */
	public DerivaUser(int userId,String email, String firstName, String lastName, String password, String createdAt,
			String updatedAt, int userType, int clientId, int authLevel) {
		super();
		this.userId = userId;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.userType = userType;
		this.clientId = clientId;
		this.authLevel = authLevel;
	}

	/**
	 * @param userId the userId to set
	 */
	protected void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * @param email the email to set
	 */
	protected void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @param firstName the firstName to set
	 */
	protected void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	protected void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @param password the password to set
	 */
	protected void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @param createdAt the createdAt to set
	 */
	protected void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	/**
	 * @param updatedAt the updatedAt to set
	 */
	protected void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	/**
	 * @param userType the userType to set
	 */
	protected void setUserType(int userType) {
		this.userType = userType;
	}
	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @return the createdAt
	 */
	public String getCreatedAt() {
		return createdAt;
	}
	/**
	 * @return the updatedAt
	 */
	public String getUpdatedAt() {
		return updatedAt;
	}
	/**
	 * @return the userType
	 */
	public int getUserType() {
		return userType;
	}
	/**
	 * @param clientId the clientId to set
	 */
	protected void setClientId(int clientId) {
		this.clientId = clientId;
	}
	/**
	 * @param authLevel the authLevel to set
	 */
	protected void setAuthLevel(int authLevel) {
		this.authLevel = authLevel;
	}
	/**
	 * @return the clientId
	 */
	public int getClientId() {
		return clientId;
	}
	/**
	 * @return the authLevel
	 */
	public int getAuthLevel() {
		return authLevel;
	}
	/**
	 * @param isValid the isValid to set
	 */
	protected void setIsValid(int isValid) {
		this.isValid = isValid;
	}
	/**
	 * @return the isValid
	 */
	public int getIsValid() {
		return isValid;
	}
	/**
	 * @return the typeName
	 */
	public String getTypeName() {
		return typeName;
	}
	/**
	 * @param typeName the typeName to set
	 */
	protected void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	/**
	 * @return the reportStartDate
	 */
	public String getReportStartDate() {
		return reportStartDate;
	}
	/**
	 * @param reportStartDate the reportStartDate to set
	 */
	public void setReportStartDate(String reportStartDate) {
		this.reportStartDate = reportStartDate;
	}
	/**
	 * @return the isDemo
	 */
	public int getIsDemo() {
		return isDemo;
	}
	/**
	 * @param isDemo the isDemo to set
	 */
	public void setIsDemo(int isDemo) {
		this.isDemo = isDemo;
	}

	public ArrayList<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(ArrayList<String> authorities) {
		this.authorities = authorities;
	}
}
