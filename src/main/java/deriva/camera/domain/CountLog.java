package deriva.camera.domain;

public class CountLog {

	private int countId;
	private int countIn;
	private int countOut;
	private int countInside;
	private String countDate;
	private String channelId;
	private String serverId;
	private String createdAt;
	
	public CountLog(int countId, int countIn, int countOut, int countInside, String countDate, String channelId,
			String serverId, String createdAt) {
		super();
		this.countId = countId;
		this.countIn = countIn;
		this.countOut = countOut;
		this.countInside = countInside;
		this.countDate = countDate;
		this.channelId = channelId;
		this.serverId = serverId;
		this.createdAt = createdAt;
	}

	public int getCountId() {
		return countId;
	}

	public void setCountId(int countId) {
		this.countId = countId;
	}

	public int getCountIn() {
		return countIn;
	}

	public void setCountIn(int countIn) {
		this.countIn = countIn;
	}

	public int getCountOut() {
		return countOut;
	}

	public void setCountOut(int countOut) {
		this.countOut = countOut;
	}

	public int getCountInside() {
		return countInside;
	}

	public void setCountInside(int countInside) {
		this.countInside = countInside;
	}

	public String getCountDate() {
		return countDate;
	}

	public void setCountDate(String countDate) {
		this.countDate = countDate;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
}
