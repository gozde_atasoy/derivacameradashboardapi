package deriva.camera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DerivaCameraDashboardApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DerivaCameraDashboardApiApplication.class, args);
	}
}
